#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>
#include <tuple>

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TDirectory.h>
#include <TKey.h>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Format.h"

#include "Core/CommonTools/interface/toolbox.h"

#include "Math/VectorUtil.h"

#include <darwin.h>

using namespace std;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

static const auto deps = numeric_limits<double>::epsilon();

////////////////////////////////////////////////////////////////////////////////
/// This function is called by `loopDirsFromGetBalance()`. It takes as argument a vector of TH3
/// which presumabilly represents the transverse momentum balance ($p_{T}^{bal}$)
/// histograms in bins of ($p_{T}^{bal}$, $p_{T}^{ave}$, $y_{probe}$) for different
/// variations of the JES. This function project the pt_balance histogram in 1-D for 
/// each bin of $p_{T}^{ave}$ and $y_{probe}$. This function calculates the histogram
/// of the mean value of the balance in bins of $p_{T}^{ave}$ for each bin, it calculates
/// the incertainty bands deduced from the statistical variations of the mean and the
/// systematics of the JES variations.
///
/// NOTE: for an explicit definition of the variables, see the additional docstring in
/// the core of the code.
/// 
/// The second arguments of this function is the directory in which the histograms are written.
/// This function creates the subdirectory structure necessary to store each series of variations
/// corresponding to each $y_{probe}$ and $p_{T}^{ave}$ bins in a given directory.
/// The structure of the output is:
/// ~~~{.cpp}
///   file/ > dOut/ > eta1/ > nominal (TH1, mean value of the $p_{T}^{bal}$ with statistical unc.)
///                         > upper (upper limit of the variations statistical+systematics)
///                         > lower (lower limit of the variations staistical + systematics)
///                         > ptbin1 > rec_any_vs_any_balance_0
///                                  > ...
///                                  > rec_any_vs_any_balance_${nVariations}
///                         > ptbin2
///                         > ...
///                         > ptbin${nPtbins}
///                 > ...
///                 > eta${nYbins}
/// ~~~
/// The structure at `file/dOut/` level is identical to the structure of the input file.
void ProcessVariations (const vector<TH3*>& variations, TDirectory& dOut)
{
    const int N = variations.size();
    Int_t nBins_local = variations.front()->GetNbinsY();

    // Filling an array with the lower edges of each bins in transverse momentum + the upper edge of the last bin.
    double LowEdges[nBins_local] = {0.};
    variations.front()->GetYaxis()->GetLowEdge(LowEdges);
    double genBins_local[nBins_local + 1];
    for (int i = 0 ; i<nBins_local ; ++i) genBins_local[i]=LowEdges[i];
    genBins_local[nBins_local] = variations.front()->GetYaxis()->GetBinUpEdge(nBins_local);

    // Iterating over the rapidity bins
    for (int y = 1 ; y <= variations.front()->GetNbinsZ() ; ++y) {

        // Create the directory "eta_bin"
        TDirectory *direta = dOut.mkdir( Form("eta%d", y) );
        cout <<"Creating directory " << bold << direta->GetPath() << normal << endl;
        direta->cd();

        // `nominal`, `upper` and `lower` contains average value of $p_{T,1}/p_{T,2}$ in bins of $p_{T,ave}$
        TH1 *nominal = new TH1F("nominal", "", nBins_local, genBins_local),
            *upper = new TH1F("upper", "", nBins_local, genBins_local),
            *lower = new TH1F("lower", "", nBins_local, genBins_local);

        // Setting the directory in which to write the histograms
        nominal->SetDirectory(direta);
        upper->SetDirectory(direta);
        lower->SetDirectory(direta);

        // Setting the title of the axis
        nominal->SetXTitle("p_{T,ave}");
        nominal->SetYTitle("#frac{1 - #LT #frac{#Delta p_{T}}{p_{T,ave}} #GT}{1 + #LT #frac{#Delta p_{T}}{p_{T,ave}} #GT }");
        upper->SetXTitle("p_{T,ave}");
        upper->SetYTitle("#frac{1 - #LT #frac{#Delta p_{T}}{p_{T,ave}} #GT}{1 + #LT #frac{#Delta p_{T}}{p_{T,ave}} #GT }");
        lower->SetXTitle("p_{T,ave}");
        lower->SetYTitle("#frac{1 - #LT #frac{#Delta p_{T}}{p_{T,ave}} #GT}{1 + #LT #frac{#Delta p_{T}}{p_{T,ave}} #GT }");

        // Iterating over the pT_Ave bins.
        for (int ipt = 1 ; ipt <= nBins_local ; ++ipt) {

            // `errUp` contains the error coming from the 'above' variations.
            // `errDn` contains the error coming from the 'under' variations.
            double errUp = variations.front()->GetBinError(ipt),
                   errDn = errUp, content=0;

            TDirectory *dirpt = direta->mkdir( Form("ptbin%d",ipt) );
            dirpt->cd();
            cout <<"Creating directory " << bold << dirpt->GetPath() << normal << endl;

            // Iterating over the variations.
            for (int sv=0 ; sv<N ; ++sv) {
                auto balance = variations[sv]->ProjectionX("py", ipt, ipt, y, y, "e");
                balance->SetName(variations[sv]->GetName());
                balance->SetTitle(variations[sv]->GetTitle());
                balance->SetDirectory(dirpt);
                double I = balance->Integral();

                if (std::abs(I)<deps) {
                    cout << "integral is null in bin (pt, eta): (" << ipt << "," << y << ") in variation: " << sv << endl;
                    continue;
                }
                else balance->Scale(1./I);

                double mean = balance->GetMean();
                double meanErr = balance->GetMeanError();

                if (sv == 0) {
                    //////////////////////////////////////////////////////////////////////////////// 
                    /// We use the terminology used in `AN2019_167_v13`:
                    ///  - The balance A is : $A = 2 \frac{p_{T,1}-p_{T,2}}{p_{T,1} + p_{T,2}} $
                    ///  - The mean balance is the quantity:
                    ///         $ R = \frac{1 - \fac{ \left\langle A \right\rangle }{2}}{1 + \frac{ \left\langle A \right\rangle }{2}}$
                    double R = (1.-mean/2.)/(1.+mean/2.),
                           Rup = (1.-(mean+meanErr)/2.)/(1.+(mean+meanErr)/2.),
                           Rdn = (1.-(mean-meanErr)/2.)/(1.+(mean-meanErr)/2.);
                    // The nominal value is set once for each bins of $p_{T,ave}$
                    // while iterating over the variations.
                    nominal->SetBinContent(ipt, R);
                    nominal->SetBinError(ipt, std::abs(Rup-Rdn)/2.);

                    // the nominal content is set once while iterating over the variations
                    content=mean;
                }
                else {
                    // Iteration over the 'non-nominal' variations (sv>1).
                    // For each variation, if the variation is above the nominal content, var-content 
                    // is added to errUp. Else var-content is added to errDn.
                    // errUp or errDn are reach via the address `err` which contain
                    // errDn or errUp depending on the need.
                    double& err = mean > content ? errUp : errDn;
                    err = hypot(err, mean - content);
                }
                balance->Write();
            }
            // See comment l.136 (close to the filling of `nominal`.
            // Content contains the nominal mean value of balance in each bins of pt.
            upper->SetBinContent(ipt, (1.-(content+errUp)/2)/(1.+(content+errUp)/2) );
            lower->SetBinContent(ipt, (1.-(content-errDn)/2)/(1.+(content-errDn)/2) );
        }
        direta->cd();
        nominal->Write();
        upper->Write();
        lower->Write();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// This function is called by `getPtAveBalance()`.
/// Recusively iterates over the internal structure of a file/directory and each
/// it fins a serie of TH3 in the file, it interpret it as the many variations 
/// of the jet energy balance and us it to calculate the mean of the balance
/// and also the balance curves in bins of pt and eta.
void loopDirsFromGetBalance (TDirectory *dIn, TDirectory * dOut)
{
    // Recursively explore the directory present in the file.
    // At each level where there are TH3, they are stored in a vector.
    vector<TH3*> variations;
    for (const auto&& obj: *(dIn->GetListOfKeys())) {
        auto const key = dynamic_cast<TKey*>(obj);
        if ( key->IsFolder() ) {
            auto ddIn = dynamic_cast<TDirectory*>( key->ReadObj() );
            if (ddIn == nullptr) {
                cerr << red << "ddIn = " << ddIn << '\n' << def;
                continue;
            }
            TDirectory * ddOut = dOut->mkdir(ddIn->GetName(), ddIn->GetTitle());
            ddOut->cd();
            cout << "creating directory " << bold << ddIn->GetPath() << normal << endl;

            loopDirsFromGetBalance(ddIn, ddOut);
        }
        else if ( ( TString( key->ReadObj()->ClassName() ).Contains("TH3") ) ) {
            auto bal3D = dynamic_cast<TH3*>( key->ReadObj() );
            bal3D->SetDirectory(0);
            variations.push_back(bal3D);
        }
        else cout << "Found " << orange << key->ReadObj()->GetName() << normal << ".. Is of "
                 << underline << key->ReadObj()->ClassName() << normal << " type, ignoring it" << endl;
    } // End of for (listof keys) loop

    //////////////////////////////////////////////////////////////////////////////// 
    /// NOTE: additional remark on the implementation:
    /// Once the TH3 representing the binned in pt_balance, eta, y (presumably in the order (x, y, z),
    /// the pt_balance is projected into a 1-D histogram for each bin of rapidity and pt_ave
    /// (this done for each of the variations). For each bin of rapidity, the mean of the pt_balance is
    /// computed (see formula in th comments of `ProcessVariations()` the standard deviations from the
    /// balance are calculated from the statistical uncertainities and from the variations (presumably
    /// the variations of the JES).
    if (variations.size() > 0)
        ProcessVariations(variations, *dOut);
}

////////////////////////////////////////////////////////////////////////////////
/// This function iterate over the histograms produced by ```getPtBalance``` and produces the
/// the momentum balance histograms in bins of ptave and eta. It calls the `loopDir()` function defined above.
void getPtAveBalance 
            (const fs::path& input, //!< input ROOT file (histograms)
             const fs::path& output, //!< name of output ROOT (histograms)
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering //!< steering parameters from `DT::Options`
            )
{
    // TODO: use config and steering (e.g. for verbosity)
    auto fIn  = make_unique<TFile>(input .c_str(), "READ"),
         fOut = make_unique<TFile>(output.c_str(), "RECREATE");
    JetEnergy::loopDirsFromGetBalance(fIn.get(), fOut.get());
}

} // end of DAS::JetEnergy namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        fs::path input, output;

        DT::Options options("Get pt average balance in bins of pt and eta.");
        options.input ("input" , &input , "input ROOT file (from `getPtBalance`)")
               .output("output", &output, "output ROOT file");

        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::JetEnergy::getPtAveBalance(input, output, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
#endif
