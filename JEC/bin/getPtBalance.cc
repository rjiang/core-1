#include <cstdlib>
#include <cassert>
#include <functional>
#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <type_traits>

#include <TROOT.h>
#include <TTree.h>
#include <TFile.h>
#include <TProfile.h>
#include <TRandom.h>
#include <TH3.h>
#include <TString.h>
#include <TDirectory.h>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Format.h"

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/variables.h"

#include "Core/JEC/interface/Scale.h"
#include "common.h"

#include "Math/VectorUtil.h"

#include <darwin.h>

using namespace std;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Apply dijet topology selection independently from the level.
template<typename Jet> bool DijetSelection
                    (const std::vector<Jet> * jets, //!< full jet vector
                     int iJES = 0, //!< JES variation index (only for rec-level)
                     const double alpha = 0.3 //!< reject significant extra jets
                    )
{
    if (jets->size() < 2) return false;

    // back-to-back
    using ROOT::Math::VectorUtil::DeltaPhi;
    if (DeltaPhi(jets->at(0).p4, jets->at(1).p4) < 2.7) return false;

    // ECAL acceptance
    if (std::abs(jets->at(0).p4.Eta()) >= 3.0 ||
        std::abs(jets->at(1).p4.Eta()) >= 3.0) return false;

    // impact of extra jets
    if (jets->size() > 2) {
        if constexpr (is_same<Jet,RecJet>()) {
            const auto pt0 = jets->at(0).CorrPt(iJES),
                       pt1 = jets->at(1).CorrPt(iJES),
                       pt2 = jets->at(2).CorrPt(iJES);
            if (pt2 > alpha*(pt0 + pt1)/2) return false;
        }
        else {
            const auto pt0 = jets->at(0).p4.Pt(),
                       pt1 = jets->at(1).p4.Pt(),
                       pt2 = jets->at(2).p4.Pt();
            if (pt2 > alpha*(pt0 + pt1)/2) return false;
        }
    }

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// Dijet balance histograms for all variations.
template<typename Jet> struct BalanceHists {

    const TString n; //!< name to indicate the selection
    vector<TH3*> hs; //!< variations

    BalanceHists (const char * name, //!< general name (will be used for the directory)
                  const vector<TString> vars //!< variation names
                 ) : n(name)
    {
        int nBins = 60;
        auto balBins = getBinning(nBins, -0.6, 0.6);
        assertValidBinning(balBins);

        for (const auto& var: vars) {
            TString name = n + '_' + var;
            hs.push_back(new TH3D(name, var, nBins, balBins.data(),
                                             nPtBins, pt_edges.data(),
                                             nYbins, y_edges.data()));
        }
    }

    void operator() (const Jet& tag, const Jet& probe, const double ev_w)
    {
        if (std::abs(tag.p4.Eta()) > 1.3) return; // barrel acceptance

        auto absy_probe = probe.AbsRap();
        for (size_t i = 0; i < hs.size(); ++i ) {

            double pt_tag, pt_probe;
            if constexpr (is_same<Jet,RecJet>()) {
                pt_tag   = tag  .CorrPt(i);
                pt_probe = probe.CorrPt(i);
            }
            else {
                pt_tag   = tag  .p4.Pt();
                pt_probe = probe.p4.Pt();
            }

            if (pt_probe < 28) continue;

            auto ptMean = (pt_tag+pt_probe)/2.0;
            auto bal    = (pt_probe-pt_tag)/ptMean;

            auto jw = tag  .weights.front()
                    * probe.weights.front();
            hs[i]->Fill(bal, ptMean, absy_probe, ev_w*jw);
        } // end of loop on variations
    }

    void Write (TDirectory * f)
    {
        f->cd();
        TDirectory * d = f->mkdir(n); // member of the class
        d->cd();
        for (TH3* h: hs) {
            h->SetDirectory(d);
            TString hname = h->GetName();
            hname.ReplaceAll(n + '_', ""); // n is a member of the class
            h->Write(hname);
        }
    }
};

vector<TString> GetScaleVariations (const DT::MetaInfo& metainfo)
{
    vector<TString> variations{"nominal"};
    const TList * groupContents = metainfo.List("variations", RecJet::ScaleVar);
    if (!groupContents)
        BOOST_THROW_EXCEPTION( invalid_argument(groupContents->GetName()) );

    for (const TObject * obj: *groupContents) {
        const auto os = dynamic_cast<const TObjString*>(obj);
        if (!os) BOOST_THROW_EXCEPTION( invalid_argument(obj->GetName()) );
        TString s = os->GetString();
        s.ReplaceAll(SysUp, "");
        s.ReplaceAll(SysDown, "");
        if (find(begin(Scale::uncs), end(Scale::uncs), s) != end(Scale::uncs))
            variations.push_back(os->GetString());
    }
    return variations;
}

////////////////////////////////////////////////////////////////////////////////
/// Get Pt balance
void getPtBalance
            (const vector<fs::path> inputs, //!< input ROOT files (n-tuples)
             const fs::path output, //!< name of output root file containing the histograms
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering, //!< steering parameters from `DT::Options`
             const DT::Slice slice = {1,0} //!< slices for running
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    unique_ptr<TChain> tIn = DT::GetChain(inputs);
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = unique_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");

    RecEvent * rEv = nullptr;
    GenEvent * gEv = nullptr;
    tIn->SetBranchAddress("recEvent", &rEv);
    if (isMC)
        tIn->SetBranchAddress("genEvent", &gEv);
    vector<RecJet> * recjets = nullptr;
    vector<GenJet> * genjets = nullptr;
    tIn->SetBranchAddress("recJets", &recjets);
    if (isMC)
        tIn->SetBranchAddress("genJets", &genjets);

    const bool applySyst = (steering & DT::syst) == DT::syst;
    const auto variations = applySyst && !isMC ? GetScaleVariations(metainfo)
                                               : vector<TString>{"nominal"};

    // detector level
    vector<BalanceHists<RecJet>> recBalances;
    recBalances.emplace_back("rec_barrel_vs_any", variations);

    // hadron level
    vector<BalanceHists<GenJet>> genBalances;
    if (isMC) 
        genBalances.emplace_back("gen_barrel_vs_any", vector<TString>{"nominal"});

    TRandom3 r3(metainfo.Seed<39856>(slice));
    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        auto gw = isMC ? gEv->weights.front() : Weight{1.,0},
             rw = rEv->weights.front();

        // we take the probe and the tag randomly
        auto r = r3.Uniform();
        int itag = (r<0.5),
            iprobe = (r>=0.5);

        if (DijetSelection<RecJet>(recjets))
        for (auto bal: recBalances)
            bal(recjets->at(itag), recjets->at(iprobe), gw*rw);

        if (!isMC) continue;

        if (DijetSelection<GenJet>(genjets))
        for (auto bal: genBalances)
            bal(genjets->at(itag), genjets->at(iprobe), gw);

    } // end of event loop

    for (auto p: recBalances) p.Write(fOut.get());
    for (auto p: genBalances) p.Write(fOut.get());

    fOut->cd();
    metainfo.Set<bool>("git", "complete", true);
    tOut->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::JetEnergy namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Get dijet pt balance in bins of pt and eta.",
                            DT::split | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::JetEnergy::getPtBalance(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }
}
#endif
