// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/SmearedJets.hh"
#include "Rivet/Projections/Smearing.hh"
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include <TTree.h>
#include <TFile.h>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <cstdlib>

using namespace std;
namespace fs = boost::filesystem;
namespace pt = boost::property_tree;


namespace Rivet {


  class RivetNtupliser : public Analysis {
    
    DAS::GenEvent * event_;
    vector<DAS::GenJet> * genJets;
    vector<DAS::RecJet> * recJets_vec;
    ////////////////////////////////////////////////////////////////////////////////
    /// output file, tree, json config, and parameters
     TFile * file; //!< output file for ntuple
     TTree * tree; 

     pt::ptree config;
     double AbsEtaMin_V, AbsEtaMax_V, pTJetMin_V, pTJetMax_V;
     bool detector_bool;
    
  public:
    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
     RIVET_DEFAULT_ANALYSIS_CTOR(RivetNtupliser);


    ////////////////////////////////////////////////////////////////////////////////
    /// Read .json configuration values, set up ttree and branches, declare fastjet finalstates
    void init() {
    const char* fname = getenv("DAS_RIVET_OUTPUT");
    assert(fname != nullptr);
    cout << "output filename: " << fname << endl; 
    file = TFile::Open(fname, "RECREATE");
    tree = new TTree("events","events");
    // Read .json configurations
    const char* cfgname = getenv("DAS_RIVET_CONFIG");
    assert(cfgname != nullptr);

    fs::path p_cfg = cfgname;
    pt::ptree config;
    pt::read_json(p_cfg.string(), config);

    // Read json Config values
    float radius = config.get<float>("radius");
    detector_bool = config.get<bool>("detector");

    cout << "\nradius: " << radius << endl;
    cout << "\nreco jets?: " << detector_bool << endl;

    auto &selection = config.get_child("selection");

    /// Get key-value pairs

    auto &AbsEtaMin_K = selection.get_child("AbsEtaMin");
    AbsEtaMin_V = AbsEtaMin_K.get_value<float>();

    auto &AbsEtaMax_K = selection.get_child("AbsEtaMax");
    AbsEtaMax_V = AbsEtaMax_K.get_value<double>();

    auto &pTJetMin_K = selection.get_child("pTJetMin");
    pTJetMin_V = pTJetMin_K.get_value<float>();

    auto &pTJetMax_K = selection.get_child("pTJetMax");
    pTJetMax_V = pTJetMax_K.get_value<float>();
    // Print out the configurations
    cout << "\n\tAbsEtaMin: " << AbsEtaMin_V << "\tAbsEtaMax: " << AbsEtaMax_V << "\tpTJetMin: " << pTJetMin_V << "\tpTJetMax: " << pTJetMax_V << endl;

    // Declare final state FastJets
      const FinalState finalstate;

    // Set up ttree branches
    event_ = new DAS::GenEvent;
    tree->Branch("genEvent", &event_);
    genJets = new vector<DAS::GenJet>;
    genJets->reserve(25);
    tree->Branch("genJets", &genJets);
    // Construct recoJets FastJets
      // if "detector":true in Rivet.json, then save recoJets as smearedjets
      
    if (detector_bool) {
      recJets_vec = new vector<DAS::RecJet>;
      recJets_vec->reserve(25);
      tree->Branch("recJets", &recJets_vec);
      }
    else {
      recJets_vec = nullptr;
    }


    // Construct genJets FastJets (declare Jets as a type)
      declare(FastJets(finalstate, FastJets::ANTIKT, radius), "Jets"); 
      if (detector_bool) {
        declare(SmearedJets(FastJets(finalstate, FastJets::ANTIKT, radius), JET_SMEAR_CMS_RUN2), "RecoJetDeclaration");
        }      

     }

    ////////////////////////////////////////////////////////////////////////////////
    /// Get FastJet final states genJets and recoJets, apply selections, fill ntuples with jet observables and weights
     void analyze(const Event &event) {
      event_->clear();
      genJets->clear();
      if (detector_bool) {
        recJets_vec->clear();
        }
      // one hepmc weight per event, in this case the phase space bias weight
      const auto& weights = event.weights();

      for (size_t i=0; i < weights.size() ; ++i) {
        float w = weights[i];
        event_->weights.push_back(DAS::Weight{w,0});
        }

      // genJets Fastjet projections
      const FastJets& fj = apply<FastJets>(event, "Jets");
      //  const Jets& RivetJets = fj.jets(Cuts::pT > pTJetMin_V*GeV && Cuts::abseta < AbsEtaMax_V);
      const Jets& RivetJets = fj.jets(Cuts::abseta < AbsEtaMax_V);
        
      for (const Jet& j : RivetJets) {
        DAS::GenJet genJet;
        genJet.p4.SetPt(j.pT());
        genJet.p4.SetEta(j.eta());
        genJet.p4.SetPhi(j.phi()); 
        genJet.p4.SetM(j.mass()); 
        genJets->push_back(genJet);
        }

      // //  SmearedJets behaves like a FastJets objects e.g. but they are

      // recoJets Fastjet projections
      if (detector_bool) {

      const SmearedJets& ApplySmearedJets = apply<SmearedJets>(event,"RecoJetDeclaration");

      const auto& SmearedJetsPostCuts = ApplySmearedJets.jets(Cuts::pT > pTJetMin_V*GeV && Cuts::abseta < AbsEtaMax_V);

      for (const auto& j : SmearedJetsPostCuts) {
        DAS::RecJet recJet;
        recJet.p4.SetPt(j.pT());
        recJet.p4.SetEta(j.eta());
        recJet.p4.SetPhi(j.phi());
        recJet.p4.SetM(j.mass());
        recJets_vec->push_back(recJet);
        }

      }

      tree->Fill();

      }

	 // Finalize
	 void finalize() {
	
        tree->Write();
        file->Close();
	 
     }

  };

  // This global object acts as a hook for the plugin system.
  RIVET_DECLARE_PLUGIN(RivetNtupliser);

}

