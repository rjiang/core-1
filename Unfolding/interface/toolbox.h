#include <iostream>

#include <TH2.h>
#include <TString.h>
#include <TDirectory.h>

namespace DAS::Unfolding {

////////////////////////////////////////////////////////////////////////////////
/// Save correlation matrix
template<typename TH2X = TH2>
void SaveCorr (const std::unique_ptr<TH2X>& cov, TString name, TDirectory * d)
{
    using namespace std;
    auto corr = unique_ptr<TH2>(dynamic_cast<TH2*>(cov->Clone(name)));
    corr->Reset();

    TString title = cov->GetTitle();
    title.ReplaceAll("covariance", "correlation");
    corr->SetTitle(title);

    const int Nx = corr->GetNbinsX(),
              Ny = corr->GetNbinsY();

    for (int x = 1; x <= Nx; ++x) {
        double denx = sqrt(cov->GetBinContent(x, x));
        for (int y = 1; y <= Ny; ++y) {
            double deny = sqrt(cov->GetBinContent(y, y));

            if (denx > 0 && deny > 0) {
                double content = cov->GetBinContent(x, y);
                content /= denx*deny;
                corr->SetBinContent(x, y, content);
                if (x != y && abs(content) >= 1.) cout << x << ' ' << y << ' ' << content << endl;
            }
            else 
                corr->SetBinContent(x, y, 0);
        }
    }
    corr->SetMinimum(-1);
    corr->SetMaximum( 1);
    corr->SetDirectory(d);
    d->cd();
    corr->Write(name);
}

} // end of DAS::Unfolding namespace
