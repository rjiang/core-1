#pragma once

#include <vector>
#include <list>
#include <optional>

#include <TUnfoldBinning.h>
#include <TTreeReaderArray.h>
#include <TH1.h>
#include <TH2.h>

#if !defined(__CLING__) || defined(__ROOTCLING__)
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Photon.h"
#include "Core/Unfolding/interface/Observable.h"
#endif

namespace DAS::Unfolding::ZmmY {

#if !defined(__CLING__) || defined(__ROOTCLING__)
struct BF : public Observable {
    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    BF ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructs a filler for the observable
    std::unique_ptr<Filler> getFiller (TTreeReader& reader) const override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Observable::setLmatrix`
    void setLmatrix (const std::unique_ptr<TH1>&, std::unique_ptr<TH2>&) override {}

    std::optional<int> process;
};

template<int PROCESS> struct BFprocessT : public BF {
    BFprocessT () { process = PROCESS; }
};

using DYJetsToMuMu   = BFprocessT<1>;
using DYJetsToTauTau = BFprocessT<2>;
using TTTo2L2Nu      = BFprocessT<3>;
using QCD            = BFprocessT<4>;
using WW             = BFprocessT<5>;
using WZ             = BFprocessT<6>;
using ZZ             = BFprocessT<7>;
static const std::vector<const char *> processes {"Z#to#mu#mu", "Z#to#tau#tau", "t#bar{t}#to2l2#nu", "QCD", "WW", "WZ", "ZZ"};
static const std::vector<const char *> channels {"#mu#mu", "#mu#mu#gamma"};

struct BFFiller final : public Filler {
    BF obs; ///< Backreference to the observable

    std::optional<TTreeReaderArray<GenMuon>> genMuons;
    std::optional<TTreeReaderArray<GenPhoton>> genPhotons;
    TTreeReaderArray<RecMuon> recMuons;
    TTreeReaderArray<RecPhoton> recPhotons;
    std::optional<TTreeReaderValue<GenEvent>> gEv;
    TTreeReaderValue<RecEvent> rEv;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    BFFiller (const BF& obs, TTreeReader& reader);

    ////////////////////////////////////////////////////////////////////////////////
    /// Makes selection on muons, but *not* on the dimuon system.
    template<typename Muon>
    bool selection (const TTreeReaderArray<Muon>& muons,
                    const Variation& v);

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillRec`
    std::list<int> fillRec (Variation&) override;

    ////////////////////////////////////////////////////////////////////////////////
    /// TODO?
    void match () override {}

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillMC`
    void fillMC (Variation&) override;

private:
    std::optional<int> irecbin;
    std::optional<float> recZW;
};
#endif

} // end of DAS::Unfolding::ZmmY namespace
