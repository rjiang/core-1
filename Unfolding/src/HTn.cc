#include "Core/Unfolding/interface/HTn.h"

#include "Core/Objects/interface/Di.h"

#include "Math/VectorUtil.h"

#include <map>
#include <limits>

#include <colours.h>
#include <exceptions.h>

#include <TVectorT.h>

using namespace std;
using namespace Eigen;

using namespace DAS;
using namespace DAS::Unfolding;
using namespace DAS::Unfolding::Rij;
namespace DE = Darwin::Exceptions;

static const auto deps = numeric_limits<double>::epsilon();

HTnFiller::HTnFiller (const HTn& obs, TTreeReader& reader)
    : obs(obs)
    , genJets(initOptionalBranch<decltype(genJets)>(reader, "genJets"))
    , recJets(reader, "recJets")
    , gEv(initOptionalBranch<decltype(gEv)>(reader, "genEvent"))
    , rEv(reader, "recEvent")
{
}

void HTnFiller::match ()
{
    matched.reset();

    auto match = [this](size_t i, size_t j) {
        const FourVector& g = genJets->At(i).p4,
                          r = recJets.At(j).p4;
        using ROOT::Math::VectorUtil::DeltaR;
        auto DR = DeltaR(g, r);
        //cout << g << '\t' << r << '\t' << DR << '\t' << result << '\n';
        return DR < obs.maxDR;
    };

    // matching (swapping leading and subleading is allowed)
    matched = genJets->GetSize() > 1 && recJets.GetSize() > 1
              && (   (match(0,0) && match(1,1))
                  || (match(0,1) && match(1,0)) );
}

namespace {

bool pass (const FourVector& jet)
{
    return jet.Pt() > 150 && std::abs(jet.Rapidity()) < 3.0;
}

template<typename Jet>
DAS::Di<const Jet, const Jet> selection (const TTreeReaderArray<Jet>& jets,
                                         const Uncertainties::Variation& v)
{
    Di<const Jet, const Jet> dijet;
    if (jets.GetSize() < 2) return dijet;

    dijet = jets.At(0) + jets.At(1);
    if (!pass(dijet.first ->CorrP4(v))
     || !pass(dijet.second->CorrP4(v)))
        dijet.clear();

    return dijet;
}

////////////////////////////////////////////////////////////////////////////////
/// Count the number of jets passing the selection and calculate corresponding
/// weight.
template<typename Jet>
pair<int,double> getMultiplicity (const TTreeReaderArray<Jet>& jets,
                                  const Uncertainties::Variation& v)
{
    int n = nJetsmin;
    double w = 1.;
    size_t N = min(jets.GetSize(), static_cast<size_t>(nJetsmax)+1);
    for (size_t i = n; i < N; ++i) {
        const Jet& jet = jets[i];
        FourVector p4 = jet.CorrP4(v);
        if (!pass(p4)) continue;
        ++n;
        w *= jet.Weight(v);
    }
    return {n,w};
}

template<typename Jet>
double getBinNumber (const DAS::Di<const Jet, const Jet>& dijet,
                     int n,
                     const Uncertainties::Variation& v,
                     TUnfoldBinning * bng)
{
    if (!dijet) return 0;
    float HT = dijet.HT(v);
    return bng->GetGlobalBinNumber(HT, n);
}

} // end of anynomous namespace

list<int> HTnFiller::fillRec (Variation& v)
{
    auto dijet = selection<RecJet>(recJets, v);
    if (!dijet) return {};

    auto [n,w] = getMultiplicity(recJets, v);
    int i = getBinNumber(dijet, n, v, obs.recBinning);
    if (i == 0) return {};

    w *= rEv->Weight(v);
    if (obs.isMC) w *= (*gEv)->Weight(v);
    w *= dijet.Weight(v);

    v.tmp->Fill(i, w);
    v.rec->Fill(i, w);

    return list<int>{i};
}

void HTnFiller::fillMC (Variation& v)
{
    if (!obs.isMC)
        BOOST_THROW_EXCEPTION( runtime_error(__func__ + " should only be called for MC"s) );

    auto recdijet = selection<RecJet>(recJets, v);
    auto [recn,recW] = getMultiplicity<RecJet>(recJets, v);
    auto irec = getBinNumber<RecJet>(recdijet, recn, v, obs.recBinning);
    recW *= rEv->Weight(v);
    if (irec > 0) recW *= recdijet.Weight(v);

    auto gendijet = selection<GenJet>(*genJets, v);
    auto [genn,genW] = getMultiplicity<GenJet>(*genJets, v);
    auto igen = getBinNumber<GenJet>(gendijet, genn, v, obs.genBinning);
    genW = (*gEv)->Weight(v);
    if (igen > 0) genW *= gendijet.Weight(v);

    if (igen > 0) v.gen->Fill(igen, genW);

    if (*matched) {
        if      (irec >  0 && igen >  0) {    v.RM->Fill(igen, irec, genW *    recW );
                                     v.missNoMatch->Fill(igen,       genW * (1-recW)); }
        else if (irec == 0 && igen >  0) v.missOut->Fill(igen,       genW           );
        else if (irec >  0 && igen == 0) v.fakeOut->Fill(      irec, genW *    recW );
    }
    else {
        if (igen > 0) v.missNoMatch->Fill(igen, genW       );
        if (irec > 0) v.fakeNoMatch->Fill(irec, genW * recW);
    }
}

////////////////////////////////////////////////////////////////////////////////

HTn::HTn () :
    Observable(__FUNCTION__, "H_{T}/2 spectra in bins of jet multiplicity")
{
    recBinning->AddAxis("HT", nRecHtBins,  recHt_edges.data(), false, false);
    recBinning->AddAxis("n" ,  nJetsBins, n_jets_edges.data(), false, false);
    genBinning->AddAxis("HT", nGenHtBins,  genHt_edges.data(), false, false);
    genBinning->AddAxis("n" ,  nJetsBins, n_jets_edges.data(), false, false);
}

unique_ptr<DAS::Unfolding::Filler> HTn::getFiller (TTreeReader& reader) const
{
    return make_unique<HTnFiller>(*this, reader);
}

unique_ptr<DAS::Unfolding::Transformer> HTn::getTransformer (TUnfoldBinning * bng) const
{
    return make_unique<HTnTransformer>(bng);
}

void HTn::setLmatrix (const unique_ptr<TH1>& bias, unique_ptr<TH2>& L)
{
    for (int n = nJetsmin; n <= nJetsmax; ++n)
    for (int iHT = 1; iHT <= nGenHtBins; ++iHT) {

        double HT = (genHt_edges.at(iHT-1) + genHt_edges.at(iHT)) / 2;
        int i = genBinning->GetGlobalBinNumber(HT, n);
        if (i == 0)
            BOOST_THROW_EXCEPTION( runtime_error("HT = "s + HT + " and n = "s + n
                                        + " do not correspond to any bin index"s) );

        // b(in)
        int               bUp     = i-nGenHtBins,
             bLeft = i-1, bCenter = i           , bRight  = i+1,
                          bDown   = i+nGenHtBins;

        // values (curvature regularisation)
        auto get = [&bias](int i) {
            auto content = bias->GetBinContent(i);
            if (content < 0)
                BOOST_THROW_EXCEPTION( DE::BadInput("Expecting only positive entries", bias) );
            return content > 0 ? 1./content : 0;
        };

        auto cUp    = get(bUp   ),
             cLeft  = get(bLeft ),
             cRight = get(bRight),
             cDown  = get(bDown );

        cout << setw(3) << n << setw(3) << iHT
             << setw(5) << bCenter << setw(15) << (cUp+cLeft+cRight+cDown)
             << setw(5) << bUp     << setw(15) << -cUp                   
             << setw(5) << bLeft   << setw(15) <<     -cLeft             
             << setw(5) << bRight  << setw(15) <<           -cRight      
             << setw(5) << bDown   << setw(15) <<                  -cDown << '\n';

        // filling L-matrix
                        L->SetBinContent(i, bCenter,  cUp+cLeft+cRight+cDown );
        if (cUp    > 0) L->SetBinContent(i, bUp    , -cUp                    );
        if (cLeft  > 0) L->SetBinContent(i, bLeft  ,     -cLeft              );
        if (cRight > 0) L->SetBinContent(i, bRight ,           -cRight       );
        if (cDown  > 0) L->SetBinContent(i, bDown  ,                  -cDown );
    }
    cout << flush;
}

////////////////////////////////////////////////////////////////////////////////

HTnTransformer::HTnTransformer (TUnfoldBinning * pre) :
    Transformer(pre, false), nHtBins(pre->GetDistributionBinning(0)->GetNoElements()-1)
{
    AddAxis(preBinning, postBinning, 0);

    postBinning->AddAxis("j", nJetsBins-1, &n_jets_edges[1] /* 3+ */, false, false);
    postBinning->AddAxis("i", nJetsBins-1, &n_jets_edges[0] /* 2+ */, false, false);
}

void HTnTransformer::Transform (const VectorXd& x) const
{
    VectorXd z = x.segment(preBinning->GetStartBin()-1,
                           preBinning->GetDistributionNumberOfBins());

    // 1) from excl mult to incl mult
    for (int n = nJetsmax; n > nJetsmin; --n)
        for (long iHt = 0; iHt < static_cast<long>(nHtBins); ++iHt)
            z(nHtBins*(n-3) + iHt) += z(nHtBins*(n-2) + iHt);
    // TODO: harmonise (either nJetsmin, or 2)

    // 2) from incl mult to ratios
    for (int j = 3; j <= nJetsmax; ++j)
    for (int i = 2; i < j; ++i) {
        for (size_t iHt = 0; iHt < nHtBins; ++iHt) {
            double num = z(nHtBins*(j-2) + iHt),
                   den = z(nHtBins*(i-2) + iHt);
            if (std::abs(den) < deps) continue;
            double Ht = postBinning->GetDistributionBinCenter(0, iHt);
            int k = postBinning->GetGlobalBinNumber(Ht, j, i);
            y(k-1) = num / den;
        }
    }
}

Ratios::Ratios () :
    Observable(__FUNCTION__, "Inclusive n-jet cross section ratios")
{
    HTn htn;
    HTnTransformer recTransformer(htn.recBinning),
                   genTransformer(htn.genBinning);
    recBinning = recTransformer.postBinning;
    genBinning = genTransformer.postBinning;

    recBinning->SetName(recBinning->GetName());
    genBinning->SetName(genBinning->GetName());
    recBinning->SetTitle(htn.recBinning->GetTitle());
    genBinning->SetTitle(htn.genBinning->GetTitle());
}
