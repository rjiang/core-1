flags ; generic flags, set at the merging of the n-tuples
{
    isMC true
    year 2016
    R 4
    labels
    {
        CHS
        HIPM
    }
}
corrections ; for event-by-event corrections
{
    PUprofile
    {
        latest ${DARWIN_TABLES}/PUprofile/Run2016.json
        Data getPUprofile.root
        MC getPUprofile.root
        maxWeight 10
    }
    jetvetomap
    {
        filename ${DARWIN_TABLES}/jetvetomaps/hotjets-UL16.root
        histname h2hot2_ul16
    }
    METfilters true
    xsection 1
    normFactor 1
    PUcleaning
    {
        MBmaxgenpt ${DARWIN_TABLES}/PUstaub/Pythia16.txt
    }
    JEStables ${DARWIN_TABLES}/JES/Summer19UL16_V7_MC
    JER
    {
        tables ${DARWIN_TABLES}/JES/Summer20UL16_JRV3_MC
        mode stochasticOnly
    }
    normalisation
    {
        turnons ${DARWIN_TABLES}/triggers/thresholds/2016/HLT_PFJet.info
        luminosities ${DARWIN_TABLES}/triggers/lumi/2016/HLT_PFJet.info
        uncertainties ${DARWIN_TABLES}/lumi_unc/FR2UL_recommended.info
        efficiencies /path/to/efficiencies/from/getTriggerTurnons
        strategy pt
        method prescales ; to normalise the events
        use_prescales true ; for the trigger efficiency curves
    }
    photons
    {
        ID Medium
        IDtable ${DARWIN_TABLES}/Egamma/Photon_Medium_UL18.root
        CSEVtable ${DARWIN_TABLES}/Egamma/Photon_CSEV_UL18.root
    }
    prefiring
    {
        file ${DARWIN_TABLES}/prefiring/JetPrefiringMapsperIOV_EOY.root
        name L1prefiring_jetpt_2016BCD
        type binned
    }
    muons
    {
        trigger_eff ${DARWIN_TABLES}/muonefficiencies/DoubleMuonTrigger_SF_UL/ScaleFactor_DoubleMuonTriggers_UL2016.root
        efficiency ${DARWIN_TABLES}/muonefficiencies/Run2/UL/2016_preVFP/Efficiency_muon_generalTracks_Run2016preVFP_UL_trackerMuon.root
        rochester
        {
            tables ${DARWIN_TABLES}/roccor/RoccoR2016.txt
            alternative false
        }
    }
}
skims
{
    dimuon
    {
        pt1 0
        pt2 0
    }
    dijet
    {
        pt1 0
        pt2 0
    }
    ZJet
    {
        zpt 0
        jpt 0
    }
}
unfolding ; for binned unfolding
{
    observables ; arbitrary number of entries, matching the namespace and class names of the relevant observables
    {
        InclusiveJet::PtY 
        DijetMass::MjjYmax
        Rij::HTn
    }
    TUnfold ; specific options to TUnfold
    {
        sysUncorr /dev/null
        regularisation /dev/null
    }
    fakeNormVar 0.05 ; relative variation of the normalisation of fake entries in the unfolding
    missNormVar 0.05 ; relative variation of the normalisation of miss entries in the unfolding
}
uncertainties
{
    toy
    {
        distribution unfold
        covariance covOutData
        level gen
        N 1000
    }
}
